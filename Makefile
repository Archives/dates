OCTAVE=octave-cli
MATLAB=`which matlab`

all: check-octave check-matlab

check-octave:
	@cd tests ;\
	$(OCTAVE) --no-init-file --silent --no-history --eval "addpath([pwd() '/../m-unit-tests/src']); runalltests" && [ ! -f ./failed ] && [ -f ./pass ]

check-matlab:
	@$(MATLAB)  -nosplash -nodisplay -r "addpath m-unit-tests/src; cd tests; runalltests; quit" && [ ! -f ./tests/failed ] && [ -f ./tests/pass ]

check-clean:
	rm -f tests/*_test_*.m tests/*.csv tests/*.xls tests/*.xlsx tests/*.mat tests/failed tests/datafile_for_test
