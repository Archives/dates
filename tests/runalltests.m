function runalltests()

% Copyright (C) 2015-2018 Dynare Team
%
% This code is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare dseries submodule is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

dates_src_root = []; % Initialize path as an array to be updated
                     % when calling initialize_dseries_toolbox() script.

opath = path();

system('rm -f failed');
system('rm -f pass');

% Initialize m-unit-test submodule
try
    initialize_unit_tests_toolbox;
catch
    error('Missing dependency: m-unit-tests module is not available.')
end

% Initialize the dates submodule
try
    initialize_dates_toolbox;
catch
    unit_tests_root = strrep(which('runalltests'),'runalltests.m','');
    addpath([unit_tests_root '../src']);
    initialize_dates_toolbox;
end

if isoctave
    more off;
end

r = run_unitary_tests_in_directory(dates_src_root(1:end-1));

delete('*.log');

if any(~[r{:,3}])
    system('touch failed');
else
    system('touch pass');
end

path(opath);

display_report(r);